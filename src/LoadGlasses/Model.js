import React, { Component } from "react";

export default class Model extends Component {
  render() {
    let { img, name, price, desc } = this.props;
    return (
      <div className="model">
        <div className="card text-white bg-dark">
          <img className="img_model" src="./glassesImage/model.jpg" alt="" />
          <img className="img-glass" src={img} alt="" />
          <div className="card-body">
            <h4 className="card-title">
              {name} <br /> {price}$
            </h4>
            <div className="note">Còn Hàng</div>
            <p className="card-text">{desc}</p>
            <button className="btn btn-danger ml-0">Thêm vào giỏ hàng</button>
          </div>
        </div>
      </div>
    );
  }
}
