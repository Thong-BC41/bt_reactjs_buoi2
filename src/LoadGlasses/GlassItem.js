import React, { Component } from "react";

export default class extends Component {
  render() {
    let { img, name, price, desc } = this.props;
    return (
      <div className="col-4 item_list">
        <div className="card p-3 glass-item-card">
          <img
            onClick={() => {
              this.props.changerGlasses(img, name, price, desc);
            }}
            className="card-img"
            src={img}
            alt=""
          />
          <div className="card-body">
            <h4 className="card-title"></h4>
            <p className="card-text"></p>
          </div>
        </div>
      </div>
    );
  }
}
