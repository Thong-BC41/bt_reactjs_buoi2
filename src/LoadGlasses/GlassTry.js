import React, { Component } from "react";
import Model from "./Model";
import ListGlass from "./ListGlass";

export default class LoadGlasses extends Component {
  state = {
    imgUrl: "./glassesImage/v9.png",
    name: "FENDI F4300",
    price: 60,
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
  };

  changerGlasses = (img, name, price, desc) => {
    this.setState({
      imgUrl: img,
      name: name,
      price: price,
      desc: desc,
    });
  };

  listGlasses = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./glassesImage/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: "./glassesImage/v2.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: "./glassesImage/v3.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 4,
      price: 70,
      name: "DIOR D6005U",
      url: "./glassesImage/v4.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 5,
      price: 40,
      name: "PRADA P8750",
      url: "./glassesImage/v5.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 6,
      price: 60,
      name: "PRADA P9700",
      url: "./glassesImage/v6.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 7,
      price: 80,
      name: "FENDI F8750",
      url: "./glassesImage/v7.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 8,
      price: 100,
      name: "FENDI F8500",
      url: "./glassesImage/v8.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 9,
      price: 60,
      name: "FENDI F4300",
      url: "./glassesImage/v9.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  ];
  render() {
    let { imgUrl, name, price, desc } = this.state;
    return (
      <div
        className="wrapper"
        style={{
          backgroundImage: `url(./glassesImage/background.jpg)`,
        }}
      >
        <div className="container">
          <h1 className="pt-4 title">TRY GLASSES APP ONLONE</h1>
          <div className="container-fluid">
            <div className="col-4">
              <Model img={imgUrl} name={name} price={price} desc={desc} />
            </div>
            <div className="col-8">
              <ListGlass
                changerGlasses={this.changerGlasses}
                listGlasses={this.listGlasses}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
