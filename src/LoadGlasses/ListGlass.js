import React, { Component } from "react";
import GlassItem from "./GlassItem";

export default class ListGlass extends Component {
  renderGlasses = (listGlasses) => {
    return listGlasses.map((glass, index) => {
      return (
        <GlassItem
          changerGlasses={this.props.changerGlasses}
          img={glass.url}
          name={glass.name}
          price={glass.price}
          desc={glass.desc}
        />
      );
    });
  };
  render() {
    let { listGlasses } = this.props;

    return (
      <div className="list-glasses">
        <div className="row">{this.renderGlasses(listGlasses)}</div>
      </div>
    );
  }
}
