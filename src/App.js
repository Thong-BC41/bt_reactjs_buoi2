import logo from "./logo.svg";
import "./App.css";
import LoadGlasses from "./LoadGlasses/GlassTry";

function App() {
  return (
    <div className="App">
      <LoadGlasses />
    </div>
  );
}

export default App;
